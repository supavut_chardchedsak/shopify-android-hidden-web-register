package com.hohapo.poc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hohapo.poc.service.RegisterApiEndpointInterface;
import com.hohapo.poc.service.response.LoginApiEndpointInterfacce;
import com.hohapo.poc.service.response.RegisterReponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Supavut-PC on 27/02/2559.
 */
public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.edt_email)
    EditText edt_email;
    @Bind(R.id.edt_password)
    EditText edt_password;
    @Bind(R.id.edt_fname)
    EditText edt_fname;
    @Bind(R.id.edt_lname)
    EditText edt_lname;

    @Bind(R.id.btn_submit)
    Button btn_submit;

    @Bind(R.id.textScroll)
    TextView textView;

    WebView webView;


    String email, password, fname, lname;

    ProgressDialog dialog;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    public static final String BASE_URL = "https://api-dev.happioteam.com:8443/eazymo/v1/";
    public static final String BASE_URL_LOGIN = "http://eazymo-dev.myshopify.com/pages/";
    Retrofit retrofit;
    RegisterApiEndpointInterface registerService;

    boolean isComplete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        ButterKnife.bind(this);
        webView = new WebView(getApplication());
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        registerService = retrofit.create(RegisterApiEndpointInterface.class);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(RegisterActivity.this);
                
                //Init Default after click register
                textView.setText("");
                isComplete = false;
                getTextRegister();

                User user = new User(email, password, fname, lname);
                if(dialog == null){
                    dialog = ProgressDialog.show(RegisterActivity.this, "", "Please wait...", true);
                }
                dialog.show();
                Log.i("user", user.toString());

                appendTextScroll("Start Register...\n");
                Call<RegisterReponse> call = registerService.createUser(user);

                call.enqueue(new Callback<RegisterReponse>() {
                    @Override
                    public void onFailure(Call<RegisterReponse> call, Throwable t) {
                        Log.e("error", t.getMessage());
                        appendTextScroll("Error :" + t.getMessage() + " ...\n");
                        dialog.hide();
                    }

                    @Override
                    public void onResponse(Call<RegisterReponse> call, Response<RegisterReponse> response) {
                        if (response.code() == 400) {
                            appendTextScroll("Bad request\n");
                            dialog.hide();
                        }else if (response.code() == 422) {
                            appendTextScroll("Request error \n");
                            dialog.hide();
                        }else if (response.code() == 429) {
                            appendTextScroll("Too many request \n");
                            dialog.hide();
                        }else if (response.code() == 201) {
                            Log.i("result", response.body().toString());
                            RegisterReponse res = response.body();
                            appendTextScroll("Register success ...\n");
                            appendTextScroll("Email : " + res.getEmail() + "\n");
                            appendTextScroll("Firstname : " + res.getFirstName() + "\n");
                            appendTextScroll("LastName : " + res.getLastName() + "\n");
                            appendTextScroll("Auto Password : " + res.getAutoPassword() + "\n");
                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.loadUrl(BASE_URL_LOGIN+"m-register?email=" + res.getEmail()
                                    + "&first_name=" + res.getFirstName()
                                    + "&last_name=" + res.getLastName()
                                    + "&auto_password=" + res.getAutoPassword());
                            webView.addJavascriptInterface(new LoadListener(), "HTMLOUT");
                            //Process Login with webview
                            new ProcessWebview().execute();

                        }else{
                            try {
                                appendTextScroll("Error :" + response.errorBody().string() + " ...\n");
                                Log.e("result", response.code() + " / " + response.errorBody().string());
                                dialog.hide();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                });
            }
        });

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
    public void appendTextScroll(String text){
        appendTextScroll(text,false);
    }
    public void appendTextScroll(String text,boolean ignore){
        Log.i("IsComplete","value : "+text+"  Status "+isComplete);
        if(!isComplete || ignore){
            textView.append(text);
        }
    }

    private void getTextRegister() {
        email = edt_email.getText().toString();
        password = edt_password.getText().toString();
        fname = edt_fname.getText().toString();
        lname = edt_lname.getText().toString();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private class ProcessWebview extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            while(!isComplete){
                try {
                    Thread.sleep(5000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                        }
                    });

                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }
            return "Executed";
        }


    }


    class LoadListener{
        @JavascriptInterface
        public void processHTML(String html)
        {
            try {
                //Log.e("result",html);
                Document doc = Jsoup.parse(html);
                Elements eles = doc.select("#response-status");
                Elements messaeEles = doc.select("#response-message");

                if(eles.size() > 0 ){
                    if(eles.get(0).text().equals("0")){
                        appendTextScroll("Wait... \n");
                    }else{
                        isComplete = true;
                        String message = messaeEles.size()>0?messaeEles.get(0).text():"";
                        appendTextScroll("response-status " + eles.get(0).text() + " \n",true);
                        appendTextScroll("response-message " + message + " \n",true);
                        appendTextScroll("Complete end process... \n",true);
                        Log.i("Success", "already Complete");
                        dialog.hide();
                    }
                }else{
                    appendTextScroll("Wait... \n");
                }
            }catch (Exception e){
                Log.e("error",e.getMessage());
            }

        }
    }
}
