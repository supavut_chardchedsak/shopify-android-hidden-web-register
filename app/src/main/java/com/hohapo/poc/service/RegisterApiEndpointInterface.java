package com.hohapo.poc.service;

import com.hohapo.poc.User;
import com.hohapo.poc.service.response.RegisterReponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Supavut on 2/28/2016.
 */
public interface RegisterApiEndpointInterface {
    @POST("auth/email/register")
    Call<RegisterReponse> createUser(@Body User user);


}
