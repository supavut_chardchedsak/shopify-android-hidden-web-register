package com.hohapo.poc.service.response;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Supavut on 2/29/2016.
 */
public interface LoginApiEndpointInterfacce {
    @GET("m-register")
    Call<ResponseBody> login(@Query("email") String email,
                                 @Query("first_name") String firstName,
                                 @Query("last_name") String lastName,
                                 @Query("auto_password") String autoPassword);


}
