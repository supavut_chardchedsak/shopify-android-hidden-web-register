package com.hohapo.poc.service.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Supavut on 2/28/2016.
 */
public class RegisterReponse {

    @SerializedName("email")
    private String email;

    @SerializedName("auto_password")
    private String autoPassword;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    public RegisterReponse(String email, String autoPassword, String firstName, String lastName) {
        this.email = email;
        this.autoPassword = autoPassword;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getAutoPassword() {
        return autoPassword;
    }

    public void setAutoPassword(String autoPassword) {
        this.autoPassword = autoPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "RegisterReponse{" +
                "email='" + email + '\'' +
                ", autoPassword='" + autoPassword + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
